const path = require('path');
const ManifestPlugin = require('webpack-manifest-plugin');

module.exports = {
  // the base path which will be used to resolve entry points
  context: path.join(__dirname, '../src'),
  // the main entry point for our application's frontend JS
  entry: [
    'babel-polyfill',
    path.join(__dirname, '../src/index.js'),
  ],

  output: {
    // the absolute path where the compiled file will be saved
    path: path.join(__dirname, '..', '..', 'bundle'),
    // if the webpack code-splitting feature is enabled, this is the path it'll use to download bundles
    publicPath: '/static/',
    library: 'App',
  },

  resolve: {
    // tell webpack which extensions to auto search when it resolves modules
    extensions: ['.js'],
  },

  plugins: [
    new ManifestPlugin({
      fileName: 'manifest.json',
      publicPath: '/static/',
      writeToFileEmit: true,
    }),
  ],

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {},
          },
        ],
      },
    ],
  },
};
