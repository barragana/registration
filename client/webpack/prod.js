const webpack = require('webpack');
const merge = require('webpack-merge');
const CompressionPlugin = require('compression-webpack-plugin');

const config = require('./config');

module.exports = merge(config, {
  output: {
    // the filename of the compiled bundle, e.g. /assets/bundle.js
    filename: '[name]-[chunkhash].js',
  },

  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      beautify: false,
      mangle: {
        screw_ie8: true,
        keep_fnames: true,
      },
      compress: {
        screw_ie8: true,
      },
      comments: false,
      exclude: '/.spec.js($|?)/i',
    }),
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.(js|css|eot|ttf|woff|woff2)$/,
    }),
  ],
});
