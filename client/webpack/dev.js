const webpack = require('webpack');
const merge = require('webpack-merge');

const config = require('./config');

module.exports = merge(config, {
  output: {
    // the filename of the compiled bundle, e.g. /assets/bundle.js
    filename: '[name].js',
  },

  // set inline-source-map to re-built it whenever any file changes
  devtool: 'inline-source-map',

  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    // activate hot reloading
    new webpack.HotModuleReplacementPlugin(),
  ],
});
