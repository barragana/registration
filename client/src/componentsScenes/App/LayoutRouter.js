import React, { Fragment } from 'react';
import { Route, Switch } from 'react-router-dom';

import { Welcome } from '../../containers';
import Footer from '../../components/Footer';
import Header from './Header';
import { Template } from './Welcome';
import DummyPage from '../../components/DummyPage';

import navItems from './navItems';

export default () => (
  <Fragment>
    <Header />
    <Template>
      <Switch>
        <Route exact path="/app/welcome" component={Welcome} />
        {
          navItems.map(item => (
            <Route key={item.to} exact path={item.to} component={DummyPage} />
          ))
        }
      </Switch>
    </Template>
    <Footer />
  </Fragment>
);
