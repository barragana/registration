export default [
  { to: '/app/for-investors', node: 'for investors' },
  { to: '/app/for-startups', node: 'for startups' },
  { to: '/app/for-experts', node: 'for experts' },
  { to: '/app/signin', node: 'sign in' },
];
