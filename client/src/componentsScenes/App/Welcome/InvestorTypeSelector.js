import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { grey300, spacingMedium, fontSize12 } from '../../../styles/variables';

import CheckboxButton from '../../../components/Checkboxes/CheckboxButton';
import Text from '../Text';

const Wrapper = styled.section`
  display: flex;
  flex-flow: wrap;
  justify-content: space-between;
  margin-bottom: ${spacingMedium};
  width: 600px;
`;

const InvestorTypeHeader = Text.extend`
  font-size: ${fontSize12};
  margin-bottom: ${spacingMedium};
  width: 100%;
`;

const Option = CheckboxButton.extend`
  padding: ${spacingMedium};
  &.individual {
    width: 30%;
  }
  &.non-institutional {
    width: 35%;
  }
  &.institution {
    width: 30%;
  }
`;

const defaultState = {
  individual: false,
  nonInstitutional: false,
  institution: false,
};

class InvestorTypeSelector extends Component {
  constructor(props) {
    super(props);
    this.state = { ...defaultState };
    this.state[props.type] = true;

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(id) {
    const nextState = { ...defaultState };
    nextState[id] = true;
    this.setState(nextState);
    this.props.handleChange(id);
  }

  render() {
    const { individual, nonInstitutional, institution } = this.state;
    return (
      <Wrapper>
        <InvestorTypeHeader className="bold-tight">I am investiment as:</InvestorTypeHeader>
        <Option
          color={grey300}
          onClick={() => { this.handleClick('individual'); }}
          checked={individual}
          className="individual"
        >
          <Text className="bold">an individual</Text>
        </Option>
        <Option
          color={grey300}
          onClick={() => { this.handleClick('nonInstitutional'); }}
          checked={nonInstitutional}
          className="non-institutional"
        >
          <Text className="bold">a non-institutional entity</Text>
          <Text>(e.g. trusts & personal LLCs)</Text>
        </Option>
        <Option
          color={grey300}
          onClick={() => { this.handleClick('institution'); }}
          checked={institution}
          className="institution"
        >
          <Text className="bold">an insitution</Text>
          <Text>(e.g. VCs, corporate, funds, family offices)</Text>
        </Option>
      </Wrapper>
    );
  }
}

InvestorTypeSelector.propTypes = {
  handleChange: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
};


export default InvestorTypeSelector;
