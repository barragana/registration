import React from 'react';
import styled from 'styled-components';

import Text from '../../Text';

import {
  spacingMedium, fontSize12
} from '../../../../styles/variables';

const Wrapper = styled.div`
  display: inline-block;
  margin-left: ${spacingMedium};
  font-size: ${fontSize12};
`;

const Info = () => (
  <Wrapper>
    <Text className="bold">I am an accredited investor</Text>
    <Text>
      By joining, you are agreeing to Propel(x){'\'s '}
      Terms of Use and Privacy Policy
    </Text>
  </Wrapper>
);

export default Info;
