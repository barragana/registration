import styled from 'styled-components';

import {
  spacingMedium, spacingSmall, fontSize12, spacingBig
} from '../../../../styles/variables';

const Section = styled.section`
  background-color: aliceblue;
  border-radius: 5px;
  display: flex;
  font-size: ${fontSize12};
  margin-bottom: ${spacingBig};
  padding: ${spacingMedium} ${spacingSmall};
  width: 580px;
`;

export default Section;
