import React from 'react';
import PropTypes from 'prop-types';

import Checkbox from '../../../../components/Checkboxes/Checkbox';
import Section from './Section';
import Info from './Info';

const PrivacyPolicy = ({ checked, onClick }) => (
  <Section>
    <Checkbox color="green" checked={checked} onClick={onClick} />
    <Info />
  </Section>
);

PrivacyPolicy.propTypes = {
  checked: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default PrivacyPolicy;
