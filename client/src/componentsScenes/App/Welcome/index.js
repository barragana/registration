import styled from 'styled-components';

import { RoundButton } from '../../../components/Buttons';
import CenteredSection from '../../../components/CenteredSection';
import {
  spacingBig, fontSize14, buttonPaddingVertical, buttonPaddingHorizontal
} from '../../../styles/variables';

export const H2 = styled.h2`
  font-weight: lighter;
  text-align: center;
`;

export const P = styled.p`
  margin-bottom: ${spacingBig};
  max-width: 850px;
  text-align: center;
`;

export const Template = CenteredSection.extend`
  flex-direction: column;
  flex-wrap: wrap;
`;

export const Button = RoundButton.extend`
  font-size: ${fontSize14};
  max-width: 300px;
  padding: ${buttonPaddingVertical} ${buttonPaddingHorizontal};
`;

export PrivacyPolicy from './PrivacyPolicy';

export InvestorType from './InvestorTypeSelector';
