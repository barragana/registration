import React from 'react';

import BaseHeader from '../../components/Header';
import Nav from '../../components/Nav';
import Logo from './Logo';

import {
  fontSize12
} from '../../styles/variables';
import navItems from './navItems';

const HeaderStyled = BaseHeader.extend`
  font-size: ${fontSize12};
  font-weight: 500;
  text-transform: capitalize;

  .logo {
    margin: 0 20px;
  }
`;

const Header = () => (
  <HeaderStyled>
    <Logo className="logo" />
    <Nav items={navItems} />
  </HeaderStyled>
);

export default Header;
