import styled from 'styled-components';

const Text = styled.p`
  margin: 0;

  &.bold {
    font-weight: bold;
    line-height: 1.8;
    margin-bottom: 5px;
  }

  &.bold-tight {
    font-weight: bold;
    line-height: 1;
  }

  &.centered {
    text-align: center;
  }
`;

export default Text;
