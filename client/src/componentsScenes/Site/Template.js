import CenteredSection from '../../components/CenteredSection';

import { fontSize52 } from '../../styles/variables';

const Template = CenteredSection.extend`
  flex-direction: column;
  flex-wrap: wrap;

  form {
    margin-top: 50px;
    position: relative;
    width: 400px;
  }

  h1, h3 {
    text-align: center;
  }

  h1 {
    font-size: ${fontSize52};
    font-weight: lighter;
  }

  h3 {
    font-weight: 400;
  }

  p {
    margin: 0 0 5px 0;
  }
}`;

export default Template;
