import React from 'react';
import LogoImg from '../../../../static/logo_propelx.png';

export default props => (
  <img width="210" height="auto" src={LogoImg} alt="logo" {...props} />
);
