export default [
  { to: '/site/invest-in-startups', node: 'invest in startups' },
  { to: '/site/list-startup', node: 'list a startup' },
  { to: '/site/share-expertise', node: 'share expertise' },
  { to: '/site/signin', node: 'sign in' },
];
