import React from 'react';
import { Link } from 'react-router-dom';
import BaseHeader from '../../components/Header';
import Nav from '../../components/Nav';
import Logo from './Logo';

import {
  headerHeightLarge, fontSize18
} from '../../styles/variables';
import navItems from './navItems';

const Header = BaseHeader.extend`
  align-items: center;
  display: flex;
  height: ${headerHeightLarge};
  font-size: ${fontSize18};
  font-weight: 600;
  justify-content: space-between;
  text-transform: uppercase;
`;

export default () => (
  <Header>
    <Link to="/site">
      <Logo />
    </Link>
    <Nav items={navItems} />
  </Header>
);
