import axios from 'axios';

export default function register(data) {
  return axios({
    method: 'POST',
    url: '/api/user/register',
    data,
  })
    .then(res => res.data);
}
