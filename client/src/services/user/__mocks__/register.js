export default function register(data) {
  return new Promise(resolve => (
    setTimeout(() => {
      delete data.password;
      delete data.passwordConfirmation;
      resolve(data);
    }, 1000)
  ));
}
