import {
  USER_REGISTER_REQUEST, USER_REGISTER_SUCCESS,
  USER_REGISTER_FAILURE, MISSING_FIELDS_WARN
} from '../../reducers/user';

import { register } from '../../services/user';

function userRequest() {
  return {
    type: USER_REGISTER_REQUEST,
  };
}

function userSucess(user) {
  return {
    type: USER_REGISTER_SUCCESS,
    user,
  };
}

function userFailure(err) {
  return {
    type: USER_REGISTER_FAILURE,
    err,
  };
}

function missingFieldsWarn(errors) {
  return {
    type: MISSING_FIELDS_WARN,
    errors,
  };
}

function validatePassword(data) {
  return data.password === data.passwordConfirmation;
}

const requiredFields = [
  { id: 'firstName', error: 'Missing First Name' },
  { id: 'lastName', error: 'Missing Last Name' },
  { id: 'email', error: 'Missing Email' },
  {
    id: 'password',
    isValid: validatePassword,
    error: 'Password is not matching or missing',
  },
];

function getErrors(data) {
  return requiredFields.reduce((result, field) => {
    if (data[field.id] && (!field.isValid || field.isValid(data))) {
      return result;
    }
    result.push(field.error);
    return result;
  }, []);
}

export default data => (
  async (dispatch) => {
    const errors = getErrors(data);
    if (errors.length) {
      return dispatch(missingFieldsWarn(errors));
    }

    dispatch(userRequest());
    try {
      const user = await register(data);
      dispatch(userSucess(user));
    } catch (e) {
      dispatch(userFailure(e));
    }
  }
);
