import {
  USER_INVESTOR_TYPE_CHANGE, USER_ACCEPT_PP_CHANGE, CLEAR_FIELD_ERRORS
} from '../../reducers/user';

export register from './register';

export const selectInvestorType = investorType => ({
  type: USER_INVESTOR_TYPE_CHANGE,
  investorType,
});

export const clearFieldErros = () => ({
  type: CLEAR_FIELD_ERRORS,
});

export const acceptPrivacyPolicy = () => ({
  type: USER_ACCEPT_PP_CHANGE,
});
