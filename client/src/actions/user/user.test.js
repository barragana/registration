import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {
  USER_REGISTER_REQUEST, USER_REGISTER_SUCCESS, MISSING_FIELDS_WARN,
  initialState
} from '../../reducers/user';
import { register } from './';

jest.mock('../../services/user/register');

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('User Actions', () => {
  it('should register and fetch user from server', async () => {
    const user = {
      firstName: 'DummyFirstName',
      lastName: 'DummyLastName',
      email: 'DummyEmail',
      password: 'DummyPassword',
      passwordConfirmation: 'DummyPassword',
    };
    const expectedActions = [
      { type: USER_REGISTER_REQUEST },
      { type: USER_REGISTER_SUCCESS, user },
    ];
    const store = mockStore({ user: { ...initialState } });

    await store.dispatch(register(user));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should not register and return errors', async () => {
    const user = {
      email: 'DummyEmail',
      password: 'DummyPassword',
      passwordConfirmation: 'DummyPassword',
    };

    const errors = [
      'Missing First Name',
      'Missing Last Name',
    ];

    const expectedActions = [
      { type: MISSING_FIELDS_WARN, errors },
    ];

    const store = mockStore({ user: { ...initialState } });
    await store.dispatch(register(user));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
