export const USER_REGISTER_REQUEST = 'USER_REGISTER_REQUEST';
export const USER_REGISTER_SUCCESS = 'USER_REGISTER_SUCCESS';
export const USER_REGISTER_FAILURE = 'USER_REGISTER_FAILURE';
export const USER_INVESTOR_TYPE_CHANGE = 'USER_INVESTOR_TYPE_CHANGE';
export const USER_ACCEPT_PP_CHANGE = 'USER_ACCEPT_PP_CHANGE';
export const MISSING_FIELDS_WARN = 'MISSING_FIELDS_WARN';
export const CLEAR_FIELD_ERRORS = 'CLEAR_FIELD_ERRORS';

export const initialState = {
  isFetching: false,
  didInvalidate: false,
  data: {
    firstName: '',
    lastName: '',
    email: '',
    investorType: 'individual',
    acceptPrivacyPolicy: false,
  },
  errors: [],
  isValid: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
  case USER_REGISTER_REQUEST:
    return {
      ...state,
      isFetching: true,
    };
  case USER_REGISTER_SUCCESS:
    return {
      ...state,
      data: { ...action.user },
      isFetching: false,
      isValid: true,
    };
  case USER_ACCEPT_PP_CHANGE:
    return {
      ...state,
      data: {
        ...state.data,
        acceptPrivacyPolicy: !state.data.acceptPrivacyPolicy,
      },
    };
  case USER_INVESTOR_TYPE_CHANGE:
    return {
      ...state,
      data: {
        ...state.data,
        investorType: action.investorType,
      },
    };
  case MISSING_FIELDS_WARN:
    return {
      ...state,
      errors: [...action.errors],
    };
  case CLEAR_FIELD_ERRORS:
    return {
      ...state,
      errors: [],
    };
  case USER_REGISTER_FAILURE:
    return {
      ...state,
      didInvalidate: true,
    };

  default:
    return state;
  }
};
