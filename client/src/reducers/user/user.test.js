import reducer, {
  USER_REGISTER_REQUEST, USER_REGISTER_FAILURE, USER_REGISTER_SUCCESS,
  USER_INVESTOR_TYPE_CHANGE, USER_ACCEPT_PP_CHANGE,
  initialState
} from './';

describe('User Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer({}, {})).toEqual({});
  });

  it('should handle USER_REGISTER_REQUEST', () => {
    const action = {
      type: USER_REGISTER_REQUEST,
    };
    expect(reducer({}, action)).toEqual({ isFetching: true });
  });

  it('should handle USER_REGISTER_SUCCESS', () => {
    const action = {
      type: USER_REGISTER_SUCCESS,
      user: { firstName: 'dummy name' },
    };
    const resultState = {
      data: action.user,
      isFetching: false,
      isValid: true,
    };

    expect(reducer({}, action)).toEqual(resultState);
  });

  it('should handle USER_REGISTER_FAILURE', () => {
    const action = {
      type: USER_REGISTER_FAILURE,
    };

    expect(reducer({}, action)).toEqual({ didInvalidate: true });
  });

  it('should handle USER_ACCEPT_PP_CHANGE', () => {
    const action = {
      type: USER_ACCEPT_PP_CHANGE,
    };

    const resultState = {
      ...initialState,
      data: {
        ...initialState.data,
        acceptPrivacyPolicy: !initialState.data.acceptPrivacyPolicy,
      },
    };
    expect(reducer(initialState, action)).toEqual(resultState);
  });

  it('should handle USER_INVESTOR_TYPE_CHANGE', () => {
    const action = {
      type: USER_INVESTOR_TYPE_CHANGE,
      investorType: 'individual',
    };

    const resultState = {
      ...initialState,
      data: {
        ...initialState.data,
        investorType: action.investorType,
      },
    };
    expect(reducer(initialState, action)).toEqual(resultState);
  });
});
