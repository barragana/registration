import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  H2, P, InvestorType, Button, PrivacyPolicy
} from '../componentsScenes/App/Welcome';

import { user } from '../actions';

class Welcome extends Component {
  constructor(props) {
    super(props);
    this.handleTUPPChange = this.handleTUPPChange.bind(this);
    this.handleInvestorTypeChange = this.handleInvestorTypeChange.bind(this);
  }

  componentDidMount() {
    if (!this.props.user.isValid) {
      this.props.history.push('/site');
    }
  }

  handleTUPPChange() {
    this.props.boundAcceptPrivacyPolicy();
  }

  handleInvestorTypeChange(type) {
    this.props.boundSelectInvestorType(type);
  }

  render() {
    const { acceptPrivacyPolicy, investorType } = this.props.user.data;
    return (
      <Fragment>
        <H2>Welcome to Propel(X)</H2>
        <P>
          Thank you for signing up. Your account is currently pending
          approval for full access to all of our investiment opportunities. By
          law, {'we\'re'} required to ask you a few questions. To expedite your
          access to deals, please answer a short survey.
        </P>
        <InvestorType handleChange={this.handleInvestorTypeChange} type={investorType} />
        <PrivacyPolicy
          checked={acceptPrivacyPolicy}
          onClick={this.handleTUPPChange}
        />
        <Button orange>Investiment Objectives Survey</Button>
      </Fragment>
    );
  }
}

Welcome.propTypes = {
  user: PropTypes.object.isRequired,
  boundSelectInvestorType: PropTypes.func.isRequired,
  boundAcceptPrivacyPolicy: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  user: { ...state.user },
});

const mapDispatchToProps = dispatch => bindActionCreators({
  boundSelectInvestorType: user.selectInvestorType,
  boundAcceptPrivacyPolicy: user.acceptPrivacyPolicy,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Welcome);
