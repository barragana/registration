/* eslint-disable no-alert */
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { user } from '../actions';

import {
  FormInput, RoundButton, FullScreenOverlay, Spinner
} from '../components';

class Register extends Component {
  static displayErrosIfNeeded(nextProps) {
    if (!nextProps.user.errors.length) return;

    alert(nextProps.user.errors.join('\n'));
    nextProps.boundClearFieldErros();
  }

  constructor(props) {
    super(props);
    this.state = {
      ...props.user.data,
      password: '',
      passwordConfirmation: '',
    };

    this.handleRegisterClick = this.handleRegisterClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    Register.displayErrosIfNeeded(nextProps);

    if (nextProps.user.isValid) {
      nextProps.history.push('/app/welcome');
    }
  }

  handleChange(e) {
    const nextState = { ...this.state };
    nextState[e.target.name] = e.target.value;
    this.setState(nextState);
  }

  handleRegisterClick(e) {
    e.preventDefault();
    this.props.boundRegister(this.state);
  }

  renderFetchingOverlayIfNeeded() {
    return !this.props.user.isFetching ? null : (
      <FullScreenOverlay>
        <Spinner />
      </FullScreenOverlay>
    );
  }

  render() {
    return (
      <Fragment>
        {this.renderFetchingOverlayIfNeeded()}
        <h1>Sign Up</h1>
        <h3>Ready to discover breakthrough startups?</h3>
        <p>Creatig an account allows you access to all of the investments opportunities on Propel(x).</p>
        <p>We recommend to sign up with LinkedIn to help personalize your Propel(x) profile.</p>
        <form>
          <FormInput type="text" name="firstName" value={this.state.firstName} placeholder="First Name" onChange={this.handleChange} />
          <FormInput type="text" name="lastName" value={this.state.lastName} placeholder="Last Name" onChange={this.handleChange} />
          <FormInput type="email" name="email" value={this.state.email} placeholder="Email" onChange={this.handleChange} />
          <FormInput type="password" name="password" value={this.state.password} placeholder="Create Password" onChange={this.handleChange} />
          <FormInput type="password" name="passwordConfirmation" value={this.state.passwordConfirmation} placeholder="Confirm Password" onChange={this.handleChange} />
          <RoundButton orange onClick={this.handleRegisterClick}>Register</RoundButton>
        </form>
      </Fragment>
    );
  }
}

Register.propTypes = {
  user: PropTypes.object.isRequired,
  boundRegister: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  user: { ...state.user },
});

const mapDispatchToProps = dispatch => bindActionCreators({
  boundRegister: user.register,
  boundClearFieldErros: user.clearFieldErros,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Register);
