import React from 'react';
import { render } from 'react-dom';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import store, { history } from './store';

import { Body } from './components';
import Site from './componentsScenes/Site';
import WebApp from './componentsScenes/App';

const App = {
  initialize: () => {
    render(
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Route component={Body}>
            <Switch>
              <Route path="/site" component={Site} />
              <Route path="/app" component={WebApp} />
              <Redirect to="/site" />
            </Switch>
          </Route>
        </ConnectedRouter>
      </Provider>,
      document.querySelector('#root'),
    );
  },
};

module.exports = App;
