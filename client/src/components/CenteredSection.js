import styled from 'styled-components';

const CenteredSection = styled.section`
  align-items: center;
  display: flex;
  justify-content: center;
  padding-bottom: 100px;
  position: relative;
}`;

export default CenteredSection;
