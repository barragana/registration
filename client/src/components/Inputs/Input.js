import styled from 'styled-components';

import {
  grey500, grey700, grey900, fontSize14,
  inputLineHeight, inputPaddingVertical, inputPaddingHorizontal
} from '../../styles/variables';

const Input = styled.input`
  border: 1px solid ${grey500};
  border-radius: 5px;
  box-sizing: border-box;
  color: ${grey900};
  font-size: ${fontSize14};
  line-height: ${inputLineHeight};
  padding: ${inputPaddingVertical} ${inputPaddingHorizontal};
  width: 100%;
  &:focus {
    outline: none;
    box-shadow: 0 0 5px ${grey700};
  }
}`;

export default Input;
