import Input from './Input';

const FormInput = Input.extend`
  display: block;
  margin-bottom: 15px;
`;

export default FormInput;
