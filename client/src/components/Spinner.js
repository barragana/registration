import React from 'react';
import styled from 'styled-components';

import { grey700 } from '../styles/variables';

const theme = (props) => {
  const color = props.color || grey700;
  const borderColor = `
    border-color: ${color} transparent ${color} transparent;
  `;

  if (props.large) {
    return `
      width: 100px;
      height: 100px;
      border: 12px solid ${color};
      ${borderColor}
      border
    `;
  }

  if (props.small) {
    return `
      width: 12px;
      height: 12px;
      border: 3px solid ${color};
      ${borderColor}
    `;
  }

  return `
    width: 60px;
    height: 60px;
    border: 8px solid ${color};
    ${borderColor}
  `;
};


const Spinner = styled.div`
  @-webkit-keyframes lds-dual-ring {
    0% {
      -webkit-transform: rotate(0);
      transform: rotate(0);
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @keyframes lds-dual-ring {
    0% {
      -webkit-transform: rotate(0);
      transform: rotate(0);
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }

  position: relative;
  div {
    position: absolute;
    ${props => theme(props)};
    border-radius: 50%;
    -webkit-animation: lds-dual-ring 1.3s linear infinite;
    animation: lds-dual-ring 1.3s linear infinite;
  }
`;

export default props => (<Spinner {...props}><div /></Spinner>);
