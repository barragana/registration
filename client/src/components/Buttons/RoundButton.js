import Button from './Button';
import {
  grey600, grey700, orange300, orange500, fontSize20
} from '../../styles/variables';

const theme = (props) => {
  if (props.orange) {
    return `
      background-color: ${orange300};
      color: white;
      &:hover { background-color: ${orange500}}
    `;
  }

  return `
    background-color: ${grey600};
    color: white;
    &:hover { background-color: ${grey700}}
  `;
};

const RoundButton = Button.extend`
  ${props => theme(props)};
  border: none;
  border-radius: 50px;
  font-size: ${fontSize20};
}`;

export default RoundButton;
