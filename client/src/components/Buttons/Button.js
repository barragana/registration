import styled from 'styled-components';

import {
  grey500, fontSize14,
  buttonPaddingVerticalLarge, buttonPaddingHorizontal
} from '../../styles/variables';

const Button = styled.button`
  border: 1px solid ${grey500};
  border-radius: 5px;
  box-sizing: border-box;
  font-family: inherit;
  font-size: ${fontSize14};
  font-weight: bolder;
  letter-spacing: 1px;
  padding: ${buttonPaddingVerticalLarge} ${buttonPaddingHorizontal};
  width: 100%;

  &:hover {
    cursor: pointer;
  }

  &:focus {
    outline: none;
    box-shadow: 0 0 7px black;
  }
}`;

export default Button;
