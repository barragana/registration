import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

const NavItem = styled(NavLink)`
  color: inherit;
  text-decoration: none;
  margin-left: 20px;
`;

export default NavItem;
