import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import NavItem from './NavItem';

const NavStyled = styled.nav`
  align-items: center;
  display: flex;
  justify-content: space-between;
`;

const Nav = ({ items }) => (
  <NavStyled>
    {
      items.map(item => (
        <NavItem key={item.to} to={item.to}>{item.node}</NavItem>
      ))
    }
  </NavStyled>
);

Nav.propTypes = {
  items: PropTypes.array.isRequired,
};

export default Nav;
