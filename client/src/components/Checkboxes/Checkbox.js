import styled from 'styled-components';
import PropTypes from 'prop-types';

import { spacingMedium } from '../../styles/variables';

export const CheckboxBehavior = styled.button`
  margin: 0;
  padding: 0;

  &:hover {
    cursor: pointer;
  }

  &:focus {
    outline: none;
    box-shadow: 0 0 5px black;
  }
`;

const Checkbox = CheckboxBehavior.extend`
  background-color: ${({ checked, color }) => (checked ? color : 'white')};
  border-radius: ${spacingMedium};
  height: ${spacingMedium};
  padding: 0;
  width: ${spacingMedium};
`;

Checkbox.propTypes = {
  checked: PropTypes.bool.isRequired,
  color: PropTypes.string.isRequired,
};

export default Checkbox;
