import PropTypes from 'prop-types';

import { CheckboxBehavior } from './Checkbox';
import { fontSize12 } from '../../styles/variables';

const CheckboxButton = CheckboxBehavior.extend`
  background-color: ${({ checked, color }) => (checked ? color : 'white')};
  border-radius: 5px;
  font-size: ${fontSize12};
`;

CheckboxButton.propTypes = {
  checked: PropTypes.bool.isRequired,
  color: PropTypes.string.isRequired,
};

export default CheckboxButton;
