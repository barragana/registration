import { injectGlobal } from 'styled-components';

import {
  grey800, fontSize52, fontSize30, fontSize24, spacingBig, spacingSmall
} from '../styles/variables';

const Body = injectGlobal`
  html, body {
    color: ${grey800};
    font-family: 'Montserrat', sans-serif;
    height: 100%;
    margin: 0;
    padding: 0;

    h1, h2 {
      margin: ${spacingBig} 0;
    }

    h1 { font-size: ${fontSize52}; }
    h2 { font-size: ${fontSize30}; }
    h3 { font-size: ${fontSize24}; margin: ${spacingSmall} 0; }
  }

  #root {
    min-height: 100%;
    position: relative;
  }
`;

export default Body;
