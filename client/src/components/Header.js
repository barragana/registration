import styled from 'styled-components';

import {
  grey600, headerHeight, spacingBig, headerPaddingHorizontal
} from '../styles/variables';

const Header = styled.header`
  align-items: center;
  border-bottom: 1px solid ${grey600};
  box-shadow: 0 0 5px ${grey600};
  display: flex;
  justify-content: space-between;
  height: ${headerHeight};
  margin-bottom: ${spacingBig};
  padding: 0 ${headerPaddingHorizontal};
`;

export default Header;
