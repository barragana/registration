import React from 'react';
import LogoImg from '../../../static/logo_propelx.png';

export default () => (
  <img width="180" height="auto" src={LogoImg} alt="logo" />
);
