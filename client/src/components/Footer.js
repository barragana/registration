import React from 'react';
import styled from 'styled-components';

import { grey300, footerHeight } from '../styles/variables';

const Footer = styled.footer`
  background-color: ${grey300};
  bottom: 0;
  height: ${footerHeight};
  line-height: ${footerHeight};
  text-align: center;
  position:absolute;
  width:100%;
`;

export default () => (
  <Footer>
    propel(x) content
  </Footer>
);
