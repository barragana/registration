module.exports = {

  // height constants
  headerHeight: '70px',
  headerHeightLarge: '100px',
  headerPaddingHorizontal: '60px',
  footerHeight: '30px',

  inputLineHeight: '1.1',
  inputPaddingVertical: '12px',
  inputPaddingHorizontal: '10px',

  buttonLineHeight: '1.8',
  buttonPaddingVertical: '16px',
  buttonPaddingHorizontal: '10px',
  buttonPaddingVerticalLarge: '24px',

  // color constants
  backgroundColor: 'white',

  // spacing
  spacingSmall: '10px',
  spacingMedium: '20px',
  spacingBig: '40px',

  // font-sizes
  fontSize10: '10px',
  fontSize12: '12px',
  fontSize14: '14px',
  fontSize16: '16px',
  fontSize18: '18px',
  fontSize20: '20px',
  fontSize24: '24px',
  fontSize26: '26px',
  fontSize30: '30px',
  fontSize52: '52px',
  fontSize70: '70px',

  // breakpoints
  breakMobile: '480px', // bp-bs-xs
  breakTablet: '768px', // bp-bs-sm
  breakDesktop: '992px', // bp-bs-md
  breakWide: '1200px', // bp-bs-lg

  // Colors
  red100: '#ffc4c4',
  red300: '#cc1212',
  red500: '#900',

  blue100: '#cce8ed',
  blue300: '#0090aa',
  blue500: '#00687a',

  green100: '#d5ecbb',
  green300: '#60ac0f',
  green500: '#457c0b',

  orange100: '#fbe0a7',
  orange300: '#f5a500',
  orange500: '#e08103',

  grey100: '#f8f8f8',
  grey200: '#f0f0f0',
  grey300: '#ededed',
  grey400: '#d5d5d5',
  grey500: '#c4bfbe',
  grey600: '#a59f9e',
  grey700: '#807877',
  grey800: '#5a5452',
  grey900: '#3b3938',
  grey1000: '#252423',

  facebookColor: '#325b99',
  facebookLightColor: '#007ccf',
  gplusColor: '#d03324',
  twitterColor: '#09aced',
  pinterestColor: '#cb2229',
  youtubeColor: '#c6312b',
  instagramColor: '#000',
  charityColor: '#4249b0',
  amazonColor: 'grey-1000',
};
