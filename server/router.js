const manifest = require('../bundle/manifest.json'); //eslint-disable-line

const {
  user,
} = require('./src/api');

module.exports = (app) => {
  app.post('/api/user/register', user.register);

  app.get('/*', (req, res) => {
    res.render('index.ejs', {
      mainCSS: manifest['main.css'],
      mainJS: manifest['main.js'],
    });
  });
};
