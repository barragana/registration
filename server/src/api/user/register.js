module.exports = async ({ body }, res, next) => {
  const user = { ...body };
  delete user.password;
  delete user.passwordConfirmation;
  setTimeout(() => {
    res.status(200).json(user);
  }, 1000);
};
