# Registration Flow
This projects aims to cover registration and welcome process of a new user.

## How it works
Registration is done only by email, being all fields required to proceed. In case, some field is not fulfilled at the time of registration, an alert (lack of time :() is displayed with missing fields warn.

### Server
There is no database configured, thus the communication with the server is only to simulating a request and an async call.

### Components
[Styled-componets](https://www.styled-components.com/) module is used in order to better organise, structure and design the style of the components. In addition, because it is very easily portable to react native.

In order of the most reusability and better organisational project structure we have shared, common components that are used and extended in different places. As well as we have components scenes, where we place those which are designed for a specific scene or page.

### State and Router
We make usage of [redux](https://redux.js.org/) and all its derivatives to manage the application state in a single source of truth. In pair with it, we use [react-router-redux](https://github.com/reactjs/react-router-redux) to handle route transitions, history and so on.

### Containers
They are main responsible of integrations redux through actions and mapping the state to props. Then passing the props to presentational components.

### Bundle
We make usage of [webpack](https://webpack.js.org/) to bundle the assets even to run in developing mode or production

### Linting Code
We use [eslint](https://eslint.org/) to make a standard code style

### Tests
So far, only [jest](https://facebook.github.io/jest/) for testing reducers and actions, which are the most important for business logic of the application. Later on it is planned to add enzyme, then start convering components tests.

## How to run it locally

   * Clone repo

   * Run `yarn install` or `npm install`

   * Run `yarn dev` or `npm run dev:npm`

   * Access `localhost:3030`

## How to run tests

   * `yarn test` or `yarn test:watch`
   * `npm run test` or `npm run test:watch`

## How to run linting

   * `yarn lint`
    
   * `npm run lint`

## Missing

  * Configuration for running in production. (Webpack part is done)

  * Due to the lack of time, it was decided to skip survey page because it consists mostly of styling and creating new components. Those capabilities can be seeing with registration and welcome page. That is why it was given more focus to make a very structured
  information flow and unit tests. Also, it was spent some time with styling and project configuration.

  * LinkedIn registration was not added because it would not be integrated and also because it does not show much value for coding proficiency
